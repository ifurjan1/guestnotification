@extends('layouts.app')

@section('htmlheader_title')
	Home
@endsection


@section('main-content')
	<div class="container spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Send Message</div>

					<div class="panel-body">

						{!! Form::open(array('action' => 'HomeController@send', 'method' => 'POST', 'id' => 'lookup', 'class' => 'form_group')) !!}


						<div class="form-group col-lg-6 ">
						  <label for="Subject">Subject:</label>
						  <input type="text" name="subject" class="form-control" id="subject" placeholder="">
						</div><br><br><br><br>
						<div class="form-group col-lg-6 ">
						  <label for="Message">Message:</label>
						  <input type="text" name="message" class="form-control" id="message" placeholder="">
						</div><br><br><br><br>
						<div class="form-group col-lg-6 ">
						  <label for="Url">Url:</label>
						  <input type="text" name="url" class="form-control" id="url" placeholder="http://www.example.com">
						</div><br><br><br><br>

						<button type="submit" class="btn btn-default center-block">Submit</button>

						{!! Form::close() !!}

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
