<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="https://github.com/acacha/adminlte-laravel"></a><b>GuestNotifier Client Platform</b></a>.
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="http://guestnotifier.com">GuestNotifier.com</a>.</strong>Created by: <a href="http://ncmedia.net">NC Media</a>.
