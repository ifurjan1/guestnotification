<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.2/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }


    public function send(Request $request)

    {

    $subject = $request->input('subject');
    $push_message = $request->input('message');
    $url = $request->input('url');
    $AppIdKey = Auth::user()->appidkey;
    $RestApiKey = Auth::user()->restapikey;


    $content = array(
      "en" => $push_message
      );
    $heading = array(
        "en" => $subject
        );
    $fields = array(
      'app_id' => $AppIdKey,
      'included_segments' => array('All'),
      //'data' => array("foo" => "bar"),
      'contents' => $content,
      'headings' => $heading,
      'url' => $url
    );

    $fields = json_encode($fields);
    // print("\nJSON sent:\n");
    print($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                           'Authorization: Basic ' . $RestApiKey));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
    $obj = json_decode($response);
    echo "broj poslanih poruka : " . $obj->recipients;


  }

  public function messages()
  {

    $user = Auth::user()->userauthkey;
    $email = Auth::user()->email;
    return view('messages')->with('user', $user)->with('email', $email);

  }


}
